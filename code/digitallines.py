from PIL import Image
import argparse
import collections
import logging
import math
import matplotlib.pyplot as plt
import numpy as np
import qpsolvers
import scipy.sparse as sp
from scipy.linalg import solveh_banded
from scipy.linalg import eigvalsh
from scipy.sparse.linalg import spsolve
from scipy.special import binom
import sys
import time


######################
######################
## PLOTTING UTILITY ##
######################
######################



PLOTTING_DIR = ""
PLOTTING_DISABLED = True

# Enable/disable plotting
def plotting_enable(direc):
    global PLOTTING_DISABLED
    global PLOTTING_DIR
    PLOTTING_DISABLED = False
    PLOTTING_DIR = direc
    logging.debug(f"Enabled plotting with directory {direc}")

# Visualise image and save to file
def plot_img(image, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.imshow(image)
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise one 1D signals and save to file
def plot_one_1d(signal, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.plot(signal, "o-")
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise two 1D signals above one another and save to file
def plot_two_1d(signal1, signal2, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.subplot(2,1,1)
    plt.plot(signal1, "o-")
    plt.subplot(2,1,2)
    plt.plot(signal2, "o-")
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise signals depending on choice
# None -> Plot both
# 'x' -> Plot first
# 'y' -> Plot second
def plot_choice_1d(signal_x, signal_y, path, choice):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    if choice is None:
        logging.debug(f"Plot choice: Both")
        plot_two_1d(signal_x, signal_y, path)
    elif choice == "x":
        logging.debug(f"Plot choice: First (X)")
        plot_one_1d(signal_x, path)
    elif choice == "y":
        logging.debug(f"Plot choice: Second (Y)")
        plot_one_1d(signal_y, path)

# Visualise a 2D signal (given separately as x and y values) with given bounds
# and save to file
def plot_2d_bounded(signal_x, signal_y, bounds, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.plot(signal_x, signal_y, ls='-', marker='o', color='#808080')
    plt.axis(bounds)
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise an image and a 2D signal (given as a list of points) and save to file
def plot_img_and_2d(image, signal_2d, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    signal_x = [x for x,_ in signal_2d]
    signal_y = [y for _,y in signal_2d]
    plt.imshow(image)
    plt.plot(signal_x, signal_y, ls='-', marker='o', c='#808080')
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise a list of 2D signals (i.e. a list of point lists) with given bounds and save to file
# (This is used exclusively for plotting the found segments)
def plot_segments(lines_points, bounds, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.axis(bounds)
    plt_x = []
    plt_y = []
    for l in lines_points:
        plt_x += [x for x,_ in l]
        plt_y += [y for _,y in l]
        plt_x.append(np.nan)
        plt_y.append(np.nan)
    plt.plot(plt_x, plt_y, ls='-', marker='o', c='#808080')
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")

# Visualise an image, a list of 2D signals (i.e. a list of point lists),
# and a list of separate points and save to file
# (This is used exclusively for plotting the found segments)
def plot_img_segments(image, lines_points, single_points, path):
    if PLOTTING_DISABLED:
        logging.debug("Plotting disabled")
        return
    npath = PLOTTING_DIR+path
    plt.imshow(image)
    for l in lines_points:
        plt.plot([x for x,_ in l], [y for _,y in l], ls='-', marker='o', c='#808080')
    xs = [x for x,_ in single_points]
    ys = [y for _,y in single_points]
    plt.plot(xs, ys, 'o', mec='#F97306', mfc='none', ms=15)
    plt.savefig(npath)
    plt.close()
    logging.debug(f"Wrote image to {npath}")



###############################
###############################
## LINE CONSTRUCTION UTILITY ##
###############################
###############################



NO_NEIGHBOUR = (-1,-1)  # error return value
# Find a neighbouring pixel of (x,y) in image that is not in the "visited" set
def neighbour(x, y, image, visited):
    neighbours = [(0,1), (0,-1), (-1,0), (1,0), (1,-1), (1,1), (-1,1), (-1,-1)]
    for nx, ny in neighbours:
        n = (x+nx,y+ny)
        if image.getpixel(n) == (0,0,0):
            if not n in visited:
                return n
    return NO_NEIGHBOUR

# Compute the squared Euclidian distance of two points
def squared_distance(tup1, tup2):
    x1, y1 = tup1
    x2, y2 = tup2
    return (x1-x2)**2 + (y1-y2)**2

# Compute the distance in the 1-norm of two points
def manhattan_distance(tup1, tup2):
    x1, y1 = tup1
    x2, y2 = tup2
    return abs(x1-x2) + abs(y1-y2)

# Finds all line segments in the image and returns a list containing for each
# found segment a list of points (this is a list of lists)
def find_segments(image):
    visited = set() # set of visited points
    lines_points = [] # elements are point lists for a certain segment
    found_points = [] # first points found for each line (this will only be used in the visualisation)

    for i in range(0, image.width):
        for j in range(0, image.height):
            # Ignore already visited and white pixels
            if (i,j) in visited:
                continue
            if image.getpixel((i,j)) != (0,0,0):
                continue

            # Unvisited pixels indicate new lines
            visited.add((i,j))
            found_points.append((i,j))

            logging.debug(f"Initial pixel {(i,j)}")

            n = neighbour(i, j, image, visited)
            
            # Edge case: Single point "line"
            if n == NO_NEIGHBOUR:
                logging.debug("No further pixels found")
                lines_points.append([(i,j)])
                continue
            
            # Otherwise, find the two ends of the line
            this_line = collections.deque([(i,j)])
            
            # Find first end
            n2 = (0,0)
            while n != NO_NEIGHBOUR:
                visited.add(n)
                this_line.append(n)
                n_i, n_j = n
                n2 = neighbour(n_i, n_j, image, visited)
                n, n2 = n2, n # if the loop stops, the last point is stored in n2

            logging.debug(f"First end {n2}")

            # Find second end
            n = neighbour(i, j, image, visited)
            n2 = (i, j)
            while n != NO_NEIGHBOUR:
                visited.add(n)
                this_line.appendleft(n)
                n_i, n_j = n
                n2 = neighbour(n_i, n_j, image, visited)
                n, n2 = n2, n # if the loop stops, the last point is stored in n2
            
            logging.debug(f"Second end {n2}")
            
            # Add constructed point list to result
            lines_points.append(list(this_line))
    
    # Before returning, plot the resulting segments if desired
    plot_img_segments(image, lines_points, found_points, "segments.png")

    return lines_points

# For a bunch of segments given as a list of point lists, determine (greedily)
# an arrangement of segments with low Euclidian distance to each other
def arrange_segments_greedy(image, point_lists, do_cyclic_line=True):
    start_points = [l[0] for l in point_lists]
    end_points = [l[-1] for l in point_lists]
    
    line_count = len(start_points)
    signal_vector = point_lists[0].copy()
    last_end = end_points[0]
    used_lines = {0}

    logging.debug(f"Arrangement step 0: Line 0, {start_points[0]} -> {end_points[0]}")
    plot_img_and_2d(image, signal_vector, "construction-step0.png")

    for i in range(1, line_count):
        
        # Find the line with the minimum distance to last_end
        # Naively calculate the argmin
        min_line = -1
        min_dir = True # True "start -> end", False "end -> start"
        min_dist = math.inf
        
        for j in range(1, line_count):
            
            # Ignore visited lines
            if j in used_lines:
                continue

            # Consider the connection last_end -> start_points[j] -> end_points[j]
            new_dist = squared_distance(start_points[j], last_end)
            if new_dist < min_dist:
                min_dist = new_dist
                min_line = j
                min_dir = True

            # Consider the connection last_end -> end_points[j] -> start_points[j]
            new_dist = squared_distance(end_points[j], last_end)
            if new_dist < min_dist:
                min_dist = new_dist
                min_line = j
                min_dir = False

        # After computation of the argmin, add the missing samples and the points of the line
        used_lines.add(min_line)
        if min_dir:
            last_start = start_points[min_line]
            last_end = end_points[min_line]
        else:
            last_start = end_points[min_line]
            last_end = start_points[min_line]
            point_lists[min_line].reverse()
        new_x, new_y = last_end
        
        logging.debug(f"Arrangement step {i}: Line {min_line}, {last_start} -> {last_end}, distance to previous {min_dist}")

        signal_vector.extend(point_lists[min_line])

        plot_img_and_2d(image, signal_vector, "construction-step{}.png".format(i))

    return signal_vector

# Pad the signal with NaNs at points of discontinuity
def signal_padding(signal, palpha, do_cyclic_line=True):
    if len(signal) == 0:
        logging.warning("Warning: Processing empty signal")
        return []

    new_signal = []
    s_prev = signal[0] # Set to signal[0] to not add missing samples in the first iteration

    for s_next in signal:
        # Two pixels are connected iff their Euclidian distance is at most sqrt(2)
        if squared_distance(s_prev, s_next) > 2:
            # If the new pixel is disconnected from the previous, add the samples
            nspaces = math.ceil(palpha * manhattan_distance(s_prev, s_next))
            new_signal.extend(nspaces * [(math.nan, math.nan)])

            logging.debug(f"Added {nspaces} corrupt samples between {s_prev} and {s_next}")
        
        new_signal.append(s_next)
        s_prev = s_next

    # If a closed line is desired, specifically check if the first and last point are connected
    if do_cyclic_line and squared_distance(signal[0], signal[-1]) > 2:
        nspaces = math.ceil(palpha * manhattan_distance(signal[0], signal[-1]))
        new_signal.extend(nspaces * [(math.nan, math.nan)])
       
        logging.debug(f"Added {nspaces} corrupt samples between {signal[-1]} and {signal[0]}")     
 
    return new_signal



###########################################
###########################################
## SOLUTIONS TO THE OPTIMIZATION PROBLEM ##
###########################################
###########################################



# Make a generalised difference matrix of given order (cyclic variant possible)
def difference_order_matrix(size, order=2, cyclic=False):
    assert order < size, "Order of the difference matrix is too big"
    rows = size if cyclic else size-order
    D = np.zeros((rows, size))
    for i in range(0, order+1):
        value = binom(order, i)
        value *= 1 if i%2==0 else -1
        for j in range(0, rows):
            D[j, (j+i)%size] = value
    return D

# Multiply a matrix with its transpose
def multiply_with_transpose(A):
    return A.T @ A

# Transform a matrix into diagonal ordered form (only the upper half)
def to_upper_diagonal_form(A, k):
    n, m = A.shape
    assert n==m, "Must enter square matrix"
    B = np.zeros((k+1, n))
    for i in range(k+1):
        B[k-i,i:] = np.diagonal(A,i)
    return B

## Solve the optimization problem as a quadratic program with a solver
## Input: the signal vector of values to be interpolated and the parameter lambda
## If set to cyclic mode, there will be a slight modification to the D'' matrix to ensure smoothness at the ends of the vector
## Output: the interpolated solution vector
def trm_solver(signal, plambda, order=2, cyclic_mode=False, solver=None):
    n = len(signal)
    logging.debug(f"Signal length: {n}")

    # Build P matrix
    D = plambda * multiply_with_transpose(difference_order_matrix(n, order, cyclic_mode))
    S = np.diag([0 if np.isnan(x) else 1 for x in signal])
    P = D + S

    # Build q vectors
    q = np.array([0 if np.isnan(x) else float(-x) for x in signal])

    # Build G matrix
    D1 = difference_order_matrix(n, 1, cyclic_mode)
    D1_rows, _ = D1.shape
    G = np.vstack((D1, -D1))

    # Constraint vector h (all twos)
    h = 2 * np.ones(2*D1_rows)

    # Throw this at a QP solver
    sol = None
    try:
        if solver is None:
            sol = qpsolvers.solve_qp(P, q, G, h)
        else:
            sol = qpsolvers.solve_qp(P, q, G, h, solver=solver)
    except ValueError as e:
        logging.error("Error while solving quadratic program: " + str(e))
        logging.debug("Computing eigenvalues")
        logging.debug(f"Eigenvalues of P: {eigvalsh(P)}")
        sys.exit(1)
    except qpsolvers.exceptions.SolverNotFound as e:
        logging.error("Solver not found: " + solver)
        logging.error("Error: " + str(e))
        sys.exit(1)

    if sol is None:
        logging.error("Quadratic solver yielded no result")
        sys.exit(1)
    if np.isnan(sol).any():
        logging.error("Quadratic solver yielded vector containing NaN values")
        sys.exit(1)
        
    return sol


################

MAX_ERROR = 1.0e-2

## Solve the optimization problem using the method by Lashgari et al. based on an augmented Lagrangian multiplier method
## Input: the signal vector of values to be interpolated and the parameter lambda
## If set to cyclic mode, there will be a slight modification to the D'' matrix to ensure smoothness at the ends of the vector
## Output: the interpolated solution vector
def trm_auglagmul(signal, plambda, order=2, cyclic_mode=False):
    n = len(signal)
    logging.debug(f"Signal length: {n}")

    sc = n if cyclic_mode else n-1 # Number of side conditions for the problem
    iterations = 0

    rho = 1
    mu = np.zeros(sc)
    r = np.zeros(n)
    r_diff = np.inf # squared norm of the differences of subsequent r-vectors (criterion for stopping the loop, initialized with infinity to not immediately stop looping)
    w = np.zeros(sc)
    w_size = np.inf # squared norm of w (initialized with infinity to not update rho during the first iteration)
    z = np.zeros(sc)
    I_tilde = np.zeros((sc,sc))
    
    # Construct the necessary matrices and vectors
    # Construct S^T*S*c (just called c) by removing the NaNs
    c = np.array([0 if np.isnan(x) else float(x) for x in signal])

    # Construct S^T*S (just called S)
    S = np.diag([0 if np.isnan(x) else 1 for x in signal])

    # Construct D' and D'' (or another generalised difference matrix)
    D1 = difference_order_matrix(n, 1, cyclic_mode)
    D = plambda * multiply_with_transpose(difference_order_matrix(n, order, cyclic_mode))

    # Then compute the solution iteratively
    while r_diff >= MAX_ERROR:
        
        # Calculate vectors w and z, as well as matrix I_tilde
        for i in range(0,sc):
            val = r[i] - r[(i+1)%n] + (mu[i]/rho)
            if val > 2:
                w[i] = r[i] - r[(i+1)%n] - 2
                z[i] = 1
                I_tilde[i][i] = 1
            elif val < -2:
                w[i] = r[i] - r[(i+1)%n] + 2
                z[i] = -1
                I_tilde[i][i] = 1
            else:
                w[i] = -mu[i]/rho
                z[i] = 0
                I_tilde[i][i] = 0

        # Update r, mu, and rho
        M = 2*S + 2*D + rho*(D1.T @ I_tilde @ D1)
        m = 2*(S @ c) - (D1.T @ I_tilde @ (mu - 2*rho*z))
        if cyclic_mode:
            r_new = spsolve(sp.csc_matrix(M), m)
        else:
            M2 = to_upper_diagonal_form(M, order)
            r_new = solveh_banded(M2, m)
        r_diff_vec = r_new - r
        r_diff = np.linalg.norm(r_diff_vec, ord=np.inf)
        r = r_new
        
        mu = I_tilde @ (mu + rho*w)
        
        w_size_new = np.inner(w,w)
        if w_size_new >= w_size:
            rho = 2*rho
        w_size = w_size_new

        iterations += 1
        logging.debug(f"Iterations: {iterations}, error (inf-norm): {r_diff}")

    return r, iterations



#################################
#################################
## LINE DISCRETIZATION UTILITY ##
#################################
#################################



# Because Python's round function is messy
def round_value(z):
    return int(math.floor(z+0.5))

# Discretize a given line
def discretize_line(xsol, ysol):
    # Add first point to list
    xres = [round_value(xsol[0])]
    yres = [round_value(ysol[0])]

    # Add other points
    for i in range(1,len(xsol)):
        x_prv, y_prv = round_value(xsol[i-1]), round_value(ysol[i-1])
        x_cur, y_cur = round_value(xsol[i]), round_value(ysol[i])
        
        dist = max(abs(x_cur - x_prv), abs(y_cur - y_prv))
        # Note: dist == 0 only if two subsequent points are the same, ignore those duplicates
        if dist == 1: # maintains 8-connectivity
            xres.append(x_cur)
            yres.append(y_cur)
        if dist == 2: # has discontinuity, must be fixed (this refers to the values of the continuous signal)
            logging.debug(f"Two-pixel-discontinuity between {(x_prv, y_prv)} and {(x_cur, y_cur)}")
            xres.append(round_value((xsol[i] + xsol[i-1])/2))
            yres.append(round_value((ysol[i] + ysol[i-1])/2))
            xres.append(x_cur)
            yres.append(y_cur)
    return xres, yres


##################
##################
## MAIN PROGRAM ## 
##################
##################



def main():
    
    #
    # ArgumentParser
    #
    
    parser = argparse.ArgumentParser(description="Reconstruct a digital line in a black and white image with the algorithm by Lashgari et al.")
    # I/O options
    parser.add_argument("-i", "--input", required=True, dest="_input", metavar="INPUT",
                        help="Specify the path to the input image you want to read")
    parser.add_argument("-o", "--output",
                        help="Specify the output path of the resulting image")
    parser.add_argument("-w", "--visualisation",
                        help="Specify an additional output path to store a detailed depiction of the reconstruction process")
    parser.add_argument("-x", "--vis-axis", choices=["x", "y"], dest="vis_axis",
                        help="In the visualisation process when two 1D signals are present, only visualize the specified axis instead of both axes (requires -w)")
    parser.add_argument("-p", "--plot", action="store_true",
                        help="Display the result using matplotlib containing the final image and the interpolated line pre-discretization")
    parser.add_argument("-t", "--time", action="store_true",
                        help="Output the time needed to perform each step of the procedure")
    verbosity_group = parser.add_mutually_exclusive_group()
    verbosity_group.add_argument("-q", "--quiet", action="store_true",
                                 help="Disable all command line output (conflicts with -v)")
    verbosity_group.add_argument("-v", "--verbose", action="count", default=0,
                                 help="Increase verbosity (max. -vv) (conflicts with -q)")
    
    # Interpolation options
    parser.add_argument("-m", "--method", choices=["alm", "qp"], default="qp",
                        help="Determine the method to be used to solve the optimization problem: Augmented Lagrangian Multipliers or Quadratic Programming (default \"qp\")")
    parser.add_argument("-s", "--solver",
                        help="When using 'qp' as the solution method, specify the solver to be used by qpsolvers, e.g. quadprog, cvxopt (see the documentation of qpsolvers), the solver needs to be installed on your system")
    parser.add_argument("-c", "--cyclic", action="store_true",
                        help="Use cyclic interpolation (conflicts with -b)")
    parser.add_argument("-b", "--bad-cyclic", action="store_true", dest="bad_cyclic",
                        help="Use bad cyclic interpolation that does not enforce smoothness conditions around the start and the end of the signal (conflicts with -c)")
    parser.add_argument("-a", "--alpha", type=float, default=1.0,
                        help="Specify the parameter alpha to be used during signal construction (default 1.0)")
    parser.add_argument("-l", "--lambda", type=float, default=1.0, dest="_lambda", metavar="LAMBDA",
                        help="Specify the parameter lambda to be used during interpolation (default 1.0)")
    parser.add_argument("-d", "--diff", type=int, default=2,
                        help="Specify the order of the difference matrix used during interpolation (default 2)")
    parser.add_argument("-e", "--epsilon", type=float, default=1.0e-2,
                        help="When using 'alm' as the solution method, specify the error bound for stopping the iteration (default 1.0e-2)")

    #
    # Parse arguments
    #

    args = parser.parse_args()

    # Check verbosity level and initialize logger
    
    log_level = logging.WARNING
    if args.quiet:
        log_level = logging.CRITICAL
    elif args.verbose == 1:
        log_level = logging.INFO
    elif args.verbose >= 2:
        log_level = logging.DEBUG

    logging.basicConfig(format='%(levelname)s: %(message)s', level=log_level)

    # Check other command-line options

    times = []

    if not args.time and not args.plot and args.visualisation is None and args.output is None:
        logging.error("At least one output option required: (-o/--output, -w/--visualisation, -p/--plot, -t/--time)")
        sys.exit(1)

    if args.visualisation is not None:
        visualisation_dir = args.visualisation
        if not visualisation_dir.endswith('/'):
            visualisation_dir += '/' # make path end in '/'
        plotting_enable(visualisation_dir)
    
    if args.vis_axis is not None and args.visualisation is None:
        logging.warning("-x/--vis-axis requires -w/--visualisation, ignoring option")
        
    if args.diff >= 10:
        logging.error("Difference order is too large (>=10)")
        sys.exit(1)
    elif args.diff <= 0:
        logging.error("Difference order must be positive")
        sys.exit(1)
    
    imethod = args.method

    if args.cyclic and args.bad_cyclic:
        logging.error("-c/--cyclic and -b/--bad-cyclic options conflict")
        sys.exit(1)

    if args._lambda <= 0:
        logging.error("Parameter lambda must be positive")
        sys.exit(1)
        
    if args.alpha <= 0:
        logging.error("Parameter alpha must be positive")
        sys.exit(1)

    if args.epsilon <= 0:
        logging.error("Parameter epsilon must be positive")
        sys.exit(1)

    global MAX_ERROR
    MAX_ERROR = args.epsilon

    logging.debug("Finished parsing command-line options")
    
    #
    # Read input image
    #

    logging.info("Reading file")
    logging.debug(f"File name {args._input}")
    
    image = Image.open(args._input)
    display_bounds = [-0.5, image.width-0.5, image.height-0.5, -0.5] # for visualisations of 2D signals
    
    logging.debug(f"display_bounds = {display_bounds}")

    # Visualise unprocessed image file
    plot_img(image, "visualisation-step1.png")

    #
    # Finding the line segments
    #
    
    logging.info("Constructing the signal vector")
    
    logging.debug("Finding segments")
    t0 = time.perf_counter()
    point_lists = find_segments(image)
    t1 = time.perf_counter()
    logging.debug(f"Time measured: {t1-t0}")
    times.append(f"Segment detection: {t1-t0}s")

    # Visualise detected segments
    plot_segments(point_lists, display_bounds, "visualisation-step2.png")

    #
    # Arranging the segments
    #

    logging.debug("Arranging segments")
    logging.debug(f"Using parameter cyclic={args.cyclic or args.bad_cyclic}")
    t0 = time.perf_counter()
    discont_signal_vector = arrange_segments_greedy(image, point_lists, args.cyclic or args.bad_cyclic)
    t1 = time.perf_counter()
    logging.debug(f"Time measured: {t1-t0}")
    times.append(f"Segment arrangement: {t1-t0}s")

    if args.bad_cyclic: # Bad cyclic lines: start and end points are the same, but use non-cyclic interpolation
        logging.debug(f"Append {discont_signal_vector[0]} for bad cyclicity")
        discont_signal_vector.append(discont_signal_vector[0])    
    discont_signal_x = [x for x,_ in discont_signal_vector]
    discont_signal_y = [y for _,y in discont_signal_vector]

    # Visualise determined point sequence within the image, combined and separated
    plot_2d_bounded(discont_signal_x, discont_signal_y, display_bounds, "visualisation-step3.png")
    plot_choice_1d(discont_signal_x, discont_signal_y, "visualisation-step4.png", args.vis_axis)

    #
    # Padding the signals with corrupted points
    #

    logging.debug("Padding signal")
    logging.debug(f"Using parameters alpha={args.alpha}, cyclic={args.cyclic}")
    t0 = time.perf_counter()
    padded_signal_vector = signal_padding(discont_signal_vector, args.alpha, args.cyclic)
    t1 = time.perf_counter()
    logging.debug(f"Time measured: {t1-t0}")
    times.append(f"Signal padding: {t1-t0}s")
    padded_signal_x = [x for x,_ in padded_signal_vector]
    padded_signal_y = [y for _,y in padded_signal_vector]

    # Visualise padded 1D signals
    plot_choice_1d(padded_signal_x, padded_signal_y, "visualisation-step5.png", args.vis_axis)

    #
    # Interpolate the signals
    #
    
    logging.info(f"Perform interpolation/approximation step (Method {imethod})")
    logging.debug(f"Using parameters lambda={args._lambda}, diff={args.diff}, cyclic={args.cyclic}")
    t0 = time.perf_counter()
    if imethod == "alm":
        interpol_x, it1 = trm_auglagmul(padded_signal_x, args._lambda, args.diff, args.cyclic)
        interpol_y, it2 = trm_auglagmul(padded_signal_y, args._lambda, args.diff, args.cyclic)
    elif imethod == "qp":
        interpol_x = trm_solver(padded_signal_x, args._lambda, args.diff, args.cyclic, args.solver)
        interpol_y = trm_solver(padded_signal_y, args._lambda, args.diff, args.cyclic, args.solver)
    else:
        logging.error(f"Invalid interpolation/approximation method ({method})")
        sys.exit(1)
    t1 = time.perf_counter()
    logging.debug(f"Time measured: {t1-t0}")
    tstr = f"Signal interpolation/approximation: {t1-t0}s"
    if imethod == "alm":
        tstr += f" (Iterations: {it1} (X), {it2} (Y))"
    times.append(tstr)
    
    # Visualise interpolated signals, separated signals and within the image
    plot_choice_1d(interpol_x, interpol_y, "visualisation-step6.png", args.vis_axis)
    plot_2d_bounded(interpol_x, interpol_y, display_bounds, "visualisation-step7.png")

    #
    # Discretize curve and produce output images
    #

    logging.info("Discretize curve")
    t0 = time.perf_counter()
    discr_x, discr_y = discretize_line(interpol_x, interpol_y)
    t1 = time.perf_counter()
    logging.debug(f"Time measured: {t1-t0}")
    times.append(f"Signal discretization: {t1-t0}s")

    logging.info("Produce output")

    res_img = Image.new('RGB', image.size, (255,255,255))
    for x, y in zip(discr_x, discr_y):
        if x >= 0 and x < image.width and y >= 0 and y < image.height:
            res_img.putpixel((x,y), (0,0,0))
        else:
            logging.debug(f'Ignored pixel ({x},{y}) in result image (out of bounds)')

    # Visualise interpolated line and resulting image 
    if args.visualisation is not None or args.plot:
        # Output of continuous interpolated line and discretized line
        plt.imshow(res_img)
        plt.plot(interpol_x, interpol_y, c='#808080', marker='o')
        if args.visualisation is not None:
            plt.savefig(visualisation_dir+"visualisation-step8.png")
        if args.plot:
            plt.show()
        plt.close()
            
    # Visualise resulting image (i.e. discretized line)
    plot_img(res_img, "visualisation-step9.png")

    # Output result file
    if args.output is not None:
        logging.debug(f"Output file name {args.output}")
        res_img.save(args.output)
    
    # Output final times (if desired)
    if args.time:
        logging.debug("Outputting times:")
        for s in times:
            print(s)

if __name__ == "__main__":
    main()
