# Rekonstruktion digitaler Linien
## Einführung
Dieses Programm ist eine Implementation des Rekonstruktionsverfahrens für digitale Linien von M. Lashgari, H. Rabbani und I. Selesnick.
Das Konsolenprogramm `digitallines.py` nimmt als Eingabe ein Schwarzweißbild und gibt die rekonstruierte Linie als Ausgabe.
## Anforderungen
`digitallines.py` hängt von den folgenden Paketen ab:
- [`pillow`](https://python-pillow.org/)
- [`numpy`](https://numpy.org/)
- [`scipy`](https://scipy.org/)
- [`matplotlib`](https://matplotlib.org/)
- [`qpsolvers`](https://github.com/stephane-caron/qpsolvers) (hat [`quadprog`](https://github.com/quadprog/quadprog) als Abhängigkeit)
## Verwendung
`digitallines.py` erlaubt die folgenden Kommandozeilenoptionen:
- `-h`, `--help` zeigt Hilfe an.
- `-i`, `--input` `INPUT` setzt `INPUT` als Pfad der zu lesenden Eingabedatei (*notwendig*).
- `-o`, `--output` `OUTPUT` setzt `OUTPUT` als Pfad für die Ausgabedatei.
- `-w`, `--visualisation` `DIR` setzt `DIR` als Verzeichnis für einen detaillierten Prozessablauf.
- `-x`, `--vis-axis` `(x|y)` beschränkt den Visualisierungsprozess auf eine Achse (x/y) (*braucht `-w`*).
- `-p`, `--plot` zeigt eine Ansicht mit `matplotlib`.
- `-t`, `--time` misst die benötigte Zeit der einzelnen Schritte (*ignoriert `-q`*).
- `-q`, `--quiet` reduziert die Ausgaben auf die Konsole (*Konflikt mit `-v`*).
- `-v`, `--verbose` erhöht die Ausgaben auf die Konsole (*auch `-vv` möglich, Konflikt mit `-q`*).
- `-m`, `--method` `(alm|qp)` bestimmt die Lösungsmethode für den Approximationsschritts, einen Ansatz über quadratische Programmierung (`qp`) oder einen Ansatz basierend auf einer verallgemeinerten Lagrange-Methode (`alm`).
- `-s`, `--solver` `SOL` bestimmt den Solver für den `qp`-Ansatz gemäß der Dokumentation von [`qpsolvers`](https://scaron.info/doc/qpsolvers/index.html). Der Solver muss installiert sein (Standardwert `quadprog`).
- `-c`, `--cyclic` rekonstruiert zyklische Linien (Konflikt mit `-b`).
- `-b`, `--bad-cyclic` rekonstruiert zyklische Linien ohne Glattheitsbedingungen an den Enden des Signals (Konflikt mit `-c`).
- `-a`, `--alpha` `NUM` setzt den Parameter Alpha (Standardwert 1.0).
- `-l`, `--lambda` `NUM` setzt den Parameter Lambda (Standardwert 1.0).
- `-d`, `--diff` `NUM` setzt die Ordnung des Regularisierungsmodells (Standardwert 2).
- `-e`, `--epsilon` `NUM` setzt die Fehlerschranke für den `alm`-Ansatz (Standardwert 0.01).
## Beispielhafte Aufrufe
Folgende Befehle aus dem `code/`-Verzeichnis heraus veranschaulichen den groben Aufbau denkbarer Aufrufe.
```
python3 digitallines.py -i ../images/circle30-1.png -o ../results/circle30-1-result.png -c -l 2.0 -p
```
```
python3 digitallines.py -i ../images/straight3.png -w ../visualisation/ -t -m alm -v
```
